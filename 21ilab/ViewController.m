//
//  ViewController.m
//  21ilab
//
//  Created by Parima Ayazi on 27/07/2017.
//  Copyright © 2017 Parima Ayazi. All rights reserved.
//

#import "ViewController.h"
#import "WebService.h"

#import <LocalAuthentication/LocalAuthentication.h>
#import <MapKit/MapKit.h>

@interface ViewController (){
    
    GMSMapView *mapView_;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"Rest your finger on the Home button.";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        // open map page
                                        [self getUserCurrentLocation];
                                        
                                        
                                    });
                                } else {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                    });
                                }
                            }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }
    
}

-(void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    mapView_.frame = self.view.frame;
}

- (void) getUserCurrentLocation {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManager.distanceFilter = 50;
    
    NSUInteger code = [CLLocationManager authorizationStatus];
    if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
        // choose one request according to your business.
        if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
            [self.locationManager  requestWhenInUseAuthorization];
        } else {
            NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
        }
    }
    
    
    [self.locationManager startUpdatingLocation];
}

- (void) getPath: (CLLocation*)currLocation{
    
    // Create a GMSCameraPosition that tells the map to display the

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:42.00
                                                            longitude:12.00
                                                                 zoom:5];
    mapView_ = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    //    self.view = mapView_;
    [self.view addSubview:mapView_];
    
    [self getLocationFromAddressString:@"Pflugstrasse 10/12, 9490 Vaduz, Liechtenstein." onSuccess:^(CLLocationCoordinate2D destination) {
        
        [[WebService get] generatePathWithSource:currLocation andDestination:destination onSuccess:^(NSDictionary *dic) {
            
            
            GMSPath *path =[GMSPath pathFromEncodedPath:dic[@"routes"][0][@"overview_polyline"][@"points"]];
            GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
            singleLine.strokeWidth = 3;
            singleLine.strokeColor = [UIColor orangeColor];
            singleLine.map = mapView_;
            
        } onError:^(NSString *err) {
            
            
        }];
        
    } onError:^(NSString *err) {
        
        
    }];
    
    
}

-(void) getLocationFromAddressString: (NSString*) addressStr
                           onSuccess: (void(^)(CLLocationCoordinate2D))onSuccess
                             onError: (void(^)(NSString*))onError {
    
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"https://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    
    [[WebService get] generateCordinateWithAddress:req onSuccess:^(NSDictionary *dic) {
        
        CLLocationCoordinate2D center;
        center.latitude=[[[dic valueForKeyPath:@"results.geometry.location.lat"] objectAtIndex:0] doubleValue];
        center.longitude = [[[dic valueForKeyPath:@"results.geometry.location.lng"] objectAtIndex:0] doubleValue];
        
        onSuccess(center);
        
    } onError:^(NSString *err) {
        NSLog(@"%@", err);
    }];
    
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
           fromLocation:(CLLocation *)oldLocation {
    CLLocation *loc = [locations lastObject];
    if(loc != nil){
        NSLog(@"latitude %1@ longitude %1@" , [NSString stringWithFormat:@"%1f", loc.coordinate.latitude], [NSString stringWithFormat:@"%1f", loc.coordinate.longitude]);
        
        [self.locationManager stopUpdatingLocation];
        
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    NSLog(@"Error while getting core location : %@",[error localizedFailureReason]);
    if ([error code] == kCLErrorDenied) {
        //you had denied
        
        
    }
    [self.locationManager stopUpdatingLocation];
}

- (void) locationManager:(CLLocationManager*)manager
     didUpdateToLocation:(CLLocation*)newLocation
            fromLocation:(CLLocation*)oldLocation {
    if(newLocation != nil){
        NSLog(@"location %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        [self.locationManager stopUpdatingLocation];
        
        [self getPath: newLocation];
        
    }
    
}

- (void) discardLocationManager {
    self.locationManager.delegate = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
