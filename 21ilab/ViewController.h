//
//  ViewController.h
//  21ilab
//
//  Created by Parima Ayazi on 27/07/2017.
//  Copyright © 2017 Parima Ayazi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>



@interface ViewController : UIViewController <CLLocationManagerDelegate, GMSMapViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *view;
@property (nonatomic , strong) CLLocationManager *locationManager;


@end

