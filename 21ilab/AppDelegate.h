//
//  AppDelegate.h
//  21ilab
//
//  Created by Parima Ayazi on 27/07/2017.
//  Copyright © 2017 Parima Ayazi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

