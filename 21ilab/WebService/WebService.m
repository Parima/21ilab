//
//  WebService.m
//  21ilab
//
//  Created by Parima Ayazi on 27/07/2017.
//  Copyright © 2017 Parima Ayazi. All rights reserved.
//

#import "WebService.h"
#import <UIKit/UIKit.h>



@implementation WebService
    
    static WebService *_instance = nil;
    
+ (WebService*) get {
    if (_instance==nil)
    _instance = [[WebService alloc] init];
    
    return _instance;
}
    
    
    
+(NSString*) formatErrorType:(NSString*)type withMessage:(NSString*)message{
    return [NSString stringWithFormat:@"%@: %@", type, message ];
}
    
- (void) performRequest:(NSMutableURLRequest*) urlRequest withMethod:(NSString*)method andJSONPostData:(NSDictionary*)pData onSuccess: (void(^)(NSDictionary*))onSuccess onError: (void(^)(NSString*))onError {
    
    //HTTP Authentication
    /*    NSString *authStr = nil;
     
     NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
     NSString *authValue = [authData base64Encoding];
     
     // Pack in the user credentials
     [urlRequest setValue:[NSString stringWithFormat:@"Basic %@", authValue] forHTTPHeaderField:@"Authorization"];
     
     // set Method
     [urlRequest setHTTPMethod:method];
     */
    //initialize a post data
    if ([urlRequest HTTPBody] == nil){
        
        if ( (pData) && [method isEqualToString:@"POST"] ) {
            
            [urlRequest setHTTPMethod:@"POST"];
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:pData
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
            
            if (jsonData) {
                //NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                // set the post data body
                [urlRequest setHTTPBody:jsonData];
                
                // set Content type
                [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            }
            
            
        }
        
    }
    [urlRequest setTimeoutInterval:150];
    // set Content type
    //[urlRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    // perform request
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                               
                               if (error != nil) {
                                   // If any error occurs then just display its description on the console.
                                   NSLog(@"TRCWebService >>  Error %@", [error localizedDescription]);
                                   // Call the error handler
                                   if (onError) {
                                       [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                           onError([WebService formatErrorType:@"Request" withMessage:[error localizedDescription]]);
                                       }];
                                       return;
                                   }
                               }
                               else{
                                   
                                   // read out answer
                                   NSDictionary *responseData = nil;
                                   NSError *errJsonify = nil;
                                   if ( data )
                                   responseData = [NSJSONSerialization JSONObjectWithData:data
                                                                                  options:0
                                                                                    error:&errJsonify];
                                   
                                   
                                   NSLog(@"TRCWebService >>  responseData %@", responseData);
                                   // if we have no data or we could not read them as JSON
                                   if ( (data==nil) || (errJsonify!=nil) ) {
                                       if (onError) {
                                           [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                               onError([WebService formatErrorType:@"Server error"
                                                                       withMessage:@"Please try again later, if the problem persists contact support."]);
                                           }];
                                           return;
                                       }
                                   }
                                   
                                   
                                   // check for error in response message
                                   BOOL error = [responseData objectForKey:@"error"];
                                   if (error){
                                       
                                       NSString *title = [responseData objectForKey:@"title"];
                                       NSString *detail = [responseData objectForKey:@"detail"];
                                       if (title && detail && onError){
                                           
                                           [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                               onError([WebService formatErrorType:title
                                                                       withMessage:detail]);
                                           }];
                                           
                                           return;
                                           
                                       }
                                   }
                                   
                                   // Call the completion handler with the returned data on the main thread.
                                   if (onSuccess) {
                                       [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                           onSuccess(responseData);
                                       }];
                                       return;
                                   }
                               }
                               
                               
                           }];
}
    
- (void) performRequestWithJSONResponse:(NSMutableURLRequest*) urlRequest withUrlEncodedPostData:(NSMutableString*)pData onSuccess: (void(^)(NSDictionary*))onSuccess onError: (void(^)(NSString*))onError
    {
        [self performRequestWithJSONResponse:urlRequest withMethod:@"POST" withUrlEncodedPostData:pData onSuccess:onSuccess onError:onError];
    }
    
- (void) generateCordinateWithAddress:(NSString *) address onSuccess:(void(^)(NSDictionary*))onSuccess onError: (void(^)(NSString*))onError
    {
        
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:address]];
        [self performRequest:urlRequest withMethod:@"POST" andJSONPostData:nil onSuccess:^(NSDictionary *dic) {
            if (onSuccess)
            onSuccess (dic);
            
        } onError:^(NSString *error) {
            if (onError)
            onError(error);
            
        }];
    }
    
    
- (void) generatePathWithSource:(CLLocation *) source andDestination: (CLLocationCoordinate2D) destination onSuccess:(void(^)(NSDictionary*))onSuccess onError: (void(^)(NSString*))onError
    {
        
        NSString *urlString = [NSString stringWithFormat:
                               @"%@?origin=%f,%f&destination=%f,%f&sensor=true&key=%@",
                               @"https://maps.googleapis.com/maps/api/directions/json",
                               source.coordinate.latitude,
                               source.coordinate.longitude,
                               destination.latitude,
                               destination.longitude,
                               @"AIzaSyBxgco6wTcQymC_iV1xdJPzLPSW2paqzD8"];
        
        // prepare POST JSON data
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [self performRequest:urlRequest withMethod:@"POST" andJSONPostData:nil onSuccess:^(NSDictionary *dic) {
            if (onSuccess)
            onSuccess (dic);
            
        } onError:^(NSString *error) {
            if (onError)
            onError(error);
            
        }];
    }
    
    @end
