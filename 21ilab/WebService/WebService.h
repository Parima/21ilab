//
//  WebService.h
//  21ilab
//
//  Created by Parima Ayazi on 27/07/2017.
//  Copyright © 2017 Parima Ayazi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <MapKit/MapKit.h>

@interface WebService : NSObject

+ (WebService*) get;

+ (NSString*) formatErrorType:(NSString*)type withMessage:(NSString*)message;

- (void) performRequestWithJSONResponse:(NSMutableURLRequest*) urlRequest withUrlEncodedPostData:(NSMutableString*)pData onSuccess: (void(^)(NSDictionary*))onSuccess onError: (void(^)(NSString*))onError;
- (void) performRequestWithJSONResponse:(NSMutableURLRequest*) urlRequest withMethod:(NSString*)method withUrlEncodedPostData:(NSMutableString*)pData onSuccess: (void(^)(NSDictionary*))onSuccess onError: (void(^)(NSString*))onError;

- (void) generateCordinateWithAddress:(NSString *) address onSuccess:(void(^)(NSDictionary*))onSuccess onError: (void(^)(NSString*))onError;
- (void) generatePathWithSource:(CLLocation *) source andDestination: (CLLocationCoordinate2D) destination onSuccess:(void(^)(NSDictionary*))onSuccess onError: (void(^)(NSString*))onError;

@end
